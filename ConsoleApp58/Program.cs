﻿using System;

namespace ConsoleApp58
{
    class Program
    {
        static void Main(string[] args)
        {
                           int[] arr = { 35, 89, 65, 311, 721, 561, 256, 16, 2, 36 };

            #region Turn Number

            foreach (int el in arr)
            {
                int i = 0;
                int a = el;
                while (a != 0)
                {
                    int rem = a % 10;
                    a = a / 10;
                    i = i * 10 + rem;
                }

                #endregion

                #region Turn Number Is Prime Or Not

                bool isPrime = true;
                if (i <= 1)
                    isPrime = false;
                else
                {
                    if (i % 2 == 0 && i != 2)
                        isPrime = false;
                    else
                    {
                        double num = Math.Sqrt(i);
                        for (int div = 3; div <= num; div += 2)
                        {
                            if (i % div == 0)
                            {
                                isPrime = false;
                                break;
                            }
                        }
                    }
                }

                #endregion

        }
    }
}
